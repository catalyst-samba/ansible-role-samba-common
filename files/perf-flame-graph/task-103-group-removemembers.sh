#!/bin/bash -x

for i in {1..50}
do
/usr/local/samba/bin/samba-tool group removemembers testgroup0 testuser$i -H ldap://$DC -UAdministrator%Password01@
done

/usr/local/samba/bin/samba-tool group delete testgroup0 -H ldap://$DC -UAdministrator%Password01@
